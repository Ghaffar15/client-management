const mongoose = require('mongoose');

const ProviderSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
}, 
{timestamps: true});

const Provider = mongoose.model('Provider', ProviderSchema);
module.exports = Provider;