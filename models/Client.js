const mongoose = require('mongoose');

const ClientSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    providers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Provider'
        }
        
    ]
}, {timestamps: true});

const Client = mongoose.model('Client', ClientSchema);

module.exports = Client;