require('dotenv').config();
const path = require('path');
const cors = require('cors');
require('./database/connect');
const express = require('express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json');
const clientRoutes = require('./routes/clientRoutes');
const providerRoutes = require('./routes/providerRoutes');

const app = express();
const router = express.Router();

app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use('/api/client', clientRoutes);
app.use('/api/provider', providerRoutes);

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
      title: 'Client Management API',
      version: '1.0.0',
      description:
        'This is a REST API to manage clients and providers',
      license: {
        name: 'Licensed Under MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
    },
    servers: [
      {
        url: 'http://localhost:4000',
        description: 'Development server',
      },
    ],
};
  
const options = {
    swaggerDefinition,
    apis: ['./routes/*.js'],
};
const swaggerSpec = swaggerJSDoc(options);

app.use(express.static(__dirname + '/frontend/dist/'));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/frontend/dist/index.html'));
});

app.listen(process.env.PORT, () => {
    console.log('Server running on port ' + process.env.PORT);
});