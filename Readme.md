# Full Stack Test (Client Management)

## Requirements
- Nodejs v12+
- NPM v6.1+
- Mongodb

## Running the application
Run in the project root
```
npm install
npm start
```

### Visit the url
```
localhost:4000
```


#### Running the frontend in dev mode
```
cd frontend
npm install
npm run serve
```

#### Visit the url
```
localhost:8080
```
