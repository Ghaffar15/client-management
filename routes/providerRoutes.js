const router = require('express').Router();
const providerValidators = require('../validations/providerValidators');
const providerController = require('../controllers/providerController');

/**
 * @swagger
 * /api/provider:
 *   get:
 *     summary: Retrieve a list of providers in the system.
 *     description: Retrieve a list of providers.
 *     responses:
 *       200:
 *         description: A list of providers.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  type: object
 *                  properties:
 *                      _id:
 *                          type: integer
 *                          description: The provider ID.
 *                          example: 6138deee33dc14974ec7fb7f
 *                      name:
 *                          type: string
 *                          description: The provider's name.
 *                          example: Ghaffaru Mudashiru
 */
router.get('', providerController.index);

/**
 * @swagger
 * /api/provider/{id}:
 *   get:
 *     summary: Retrieve a single provider.
 *     description: Retrieve a single provider.
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            description: ID of the provider
 *            schema:
 *              type: string  
 *     responses:
 *       200:
 *         description: A single provider.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  _id:
 *                      type: string
 *                      description: The provider id.
 *                      example: 6138deee33dc14974ec7fb7f
 *                  name:
 *                      type: string
 *                      description: The client's name.
 *                      example: Ghaffaru Mudashiru
 *       404:
 *          description: Provider not found error!.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                              example: Provider not found!
 *                      
*/
router.get('/:id', providerController.show);

/**
 * @swagger
 * /api/provider/{id}:
 *   patch:
 *     summary: Updates a provider.
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            description: ID of the provider
 *            schema:
 *              type: string 
 *     requestBody:
 *          required: false
 *          content:
 *           application/json:
 *            schema:
 *             type: object
 *             properties:
 *              name:
 *               type: string
 *               description: The provider name
 *               example: Ghaffaru Mudashiru
 *     responses:
 *       200:
 *         description: Updated
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                   message:
 *                       type: string
 *                       example: Provider updated successfully!
 *                   client:
 *                       type: object
 *                       description: The provider's data.
 *                       properties:
 *                          _id:
 *                              type: string
 *                              description: The provider ID
 *                              example: 6138deee33dc14974ec7fb7f
 *                          name:
 *                              type: string
 *                              description: The provider name
 *                              example: Ghaffaru Mudashiru
 *
*/
router.patch('/:id', providerController.update);

/**
 * @swagger
 * /api/provider/{id}:
 *   delete:
 *     summary: Deletes a single provider.
 *     description: Retrieve a single provider.
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            description: ID of the provider
 *            schema:
 *              type: string  
 *     responses:
 *       200:
 *         description: A single provider.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                      type: string
 *                      example: Provider deleted successfully
 *       404:
 *          description: Provider not found error!.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                              example: Provider not found!
 *                      
*/
router.delete('/:id', providerController.destroy);

/**
 * @swagger
 * /api/provider:
 *   post:
 *     summary: Create a new provider.
 *     requestBody:
 *          required: true
 *          content:
 *           application/json:
 *            schema:
 *             type: object
 *             properties:
 *              name:
 *               type: string
 *               description: The provider name
 *               example: Ghaffaru Mudashiru
 *     responses:
 *       200:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                   message:
 *                       type: string
 *                       description: The user ID.
 *                       example: Provider created successfully!
 *                   client:
 *                       type: object
 *                       description: The provider's data.
 *                       properties:
 *                          _id:
 *                              type: string
 *                              description: The provider ID
 *                              example: 6138deee33dc14974ec7fb7f
 *                          name:
 *                              type: string
 *                              description: The provider name
 *                              example: Ghaffaru Mudashiru
 *
*/
router.post('', providerValidators.createNewProvider, providerController.store);

module.exports = router;