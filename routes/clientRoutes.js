const { Router } = require('express');
const clientValidators = require('../validations/clientValidators');
const clientController = require('../controllers/clientController');
const router = Router();

/**
 * @swagger
 * /api/client:
 *   get:
 *     summary: Retrieve a list of clients in the system.
 *     description: Retrieve a list of clients populated with their providers.
 *     responses:
 *       200:
 *         description: A list of clients.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  type: object
 *                  properties:
 *                      _id:
 *                          type: string
 *                          description: The client ID.
 *                          example: 6138deee33dc14974ec7fb7f
 *                      name:
 *                          type: string
 *                          description: The client's name.
 *                          example: Ghaffaru Mudashiru
 *                      email:
 *                          type: string
 *                          description: The client's email.
 *                          example: mudashiruagm@gmail.com
 *                      phone:
 *                          type: string
 *                          description: The client's phone
 *                          example: 0241992669
 *                      providers:
 *                          type: array
 *                          description: The client's providers
 *                          example: [6138deee33dc14974ec7fb7f,  6138deee33dc14974ec7fb7f]
 */
router.get('', clientController.index);

/**
 * @swagger
 * /api/client/{id}:
 *   get:
 *     summary: Retrieve a single client.
 *     description: Retrieve a single client with providers.
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            description: ID of the client
 *            schema:
 *              type: string  
 *     responses:
 *       200:
 *         description: A single client.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  _id:
 *                      type: string
 *                      description: The client id.
 *                      example: 6138deee33dc14974ec7fb7f
 *                  name:
 *                      type: string
 *                      description: The client's name.
 *                      example: Ghaffaru Mudashiru
 *                  email:
 *                      type: string
 *                      description: The client's email.
 *                      example: mudashiruagm@gmail.com
 *                  phone:
 *                      type: string
 *                      description: The client's phone.
 *                      example: 0241992559
 *                  providers:
 *                      type: array
 *                      description: The client providers
 *                      example: [6138deee33dc14974ec7fb7f,  6138deee33dc14974ec7fb7f]
 *       404:
 *          description: Client not found error!.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                              example: Client not found!
 *                      
*/
router.get('/:id', clientController.show);
/**
 * @swagger
 * /api/client/{id}:
 *   patch:
 *     summary: Updates a client.
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            description: ID of the client
 *            schema:
 *              type: string 
 *     requestBody:
 *          required: false
 *          content:
 *           application/json:
 *            schema:
 *             type: object
 *             properties:
 *              name:
 *               type: string
 *               description: The client name
 *               example: Ghaffaru Mudashiru
 *              email:
 *               type: string
 *               description: The client email
 *               example: mudashiruagm@gmail.com
 *              phone:
 *               type: string
 *               description: The client phone
 *               example: 02323232
 *              providers:
 *               type: array
 *               description: The client providers
 *               example: [6138deee33dc14974ec7fb7f,  6138deee33dc14974ec7fb7d]
 *     responses:
 *       200:
 *         description: Updated
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                   message:
 *                       type: string
 *                       example: Client updated successfully!
 *                   client:
 *                       type: object
 *                       description: The client's data.
 *                       properties:
 *                          _id:
 *                              type: string
 *                              description: The client ID
 *                              example: 6138deee33dc14974ec7fb7f
 *                          name:
 *                              type: string
 *                              description: The client name
 *                              example: Ghaffaru Mudashiru
 *                          email:
 *                              type: string
 *                              description: The client email
 *                              example: mudashiruagm@gmail.com
 *                          phone:
 *                              type: string
 *                              description: The client phone
 *                              example: 02329032323
 *                          providers: 
 *                              type: array
 *                              example: [6138deee33dc14974ec7fb7f,  6138deee33dc14974ec7fb7f]
 *
*/
router.patch('/:id', clientController.update);

/**
 * @swagger
 * /api/client/{id}:
 *   delete:
 *     summary: Deletes a single client.
 *     description: Retrieve a single client with providers.
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            description: ID of the client
 *            schema:
 *              type: string  
 *     responses:
 *       200:
 *         description: A single client.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                      type: string
 *                      example: Client deleted successfully
 *       404:
 *          description: Client not found error!.
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                              example: Client not found!
 *                      
*/
router.delete('/:id', clientController.destroy);

/**
 * @swagger
 * /api/client:
 *   post:
 *     summary: Create a new client.
 *     requestBody:
 *          required: true
 *          content:
 *           application/json:
 *            schema:
 *             type: object
 *             properties:
 *              name:
 *               type: string
 *               description: The client name
 *               example: Ghaffaru Mudashiru
 *              email:
 *               type: string
 *               description: The client email
 *               example: mudashiruagm@gmail.com
 *              phone:
 *               type: string
 *               description: The client phone
 *               example: 02323232
 *              providers:
 *               type: array
 *               description: The client providers
 *               example: [6138deee33dc14974ec7fb7f,  6138deee33dc14974ec7fb7d]
 *     responses:
 *       200:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                   message:
 *                       type: string
 *                       description: The user ID.
 *                       example: Client created successfully!
 *                   client:
 *                       type: object
 *                       description: The client's data.
 *                       properties:
 *                          _id:
 *                              type: string
 *                              description: The client ID
 *                              example: 6138deee33dc14974ec7fb7f
 *                          name:
 *                              type: string
 *                              description: The client name
 *                              example: Ghaffaru Mudashiru
 *                          email:
 *                              type: string
 *                              description: The client email
 *                              example: mudashiruagm@gmail.com
 *                          phone:
 *                              type: string
 *                              description: The client phone
 *                              example: 02329032323
 *                          providers: 
 *                              type: array
 *                              example: [6138deee33dc14974ec7fb7f,  6138deee33dc14974ec7fb7f]
 *
*/
router.post('', clientValidators.createNewClient, clientController.newClient);

module.exports = router;