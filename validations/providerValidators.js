const { body, validationResult } = require('express-validator');
const { returnErrors } = require('./helper');

exports.createNewProvider = [
    body('name').notEmpty().withMessage('Provider name is required!'),

    (req, res, next) => {

      
        returnErrors(validationResult(req), res, next);

        // next();
    }
]; 