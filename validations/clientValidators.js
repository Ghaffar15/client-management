const { body, validationResult } = require('express-validator');
const { returnErrors } = require('./helper');

exports.createNewClient = [
    body('name').notEmpty().withMessage('Client name is required!'),
    body('email').notEmpty().withMessage('Client email is required!'),
    body('email').isEmail().withMessage('Client email is not a valid email'),
    body('phone').notEmpty().withMessage('Client phone number is required!'),
    body('providers').notEmpty().withMessage('Providers are required'),
    body('providers').isArray().withMessage('Providers must be an array!'),
    (req, res, next) => {

        returnErrors(validationResult(req), res, next);
    }
]