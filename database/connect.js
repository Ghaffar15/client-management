const mongoose = require("mongoose");

let connectToDb = async () => {
    try {
        await mongoose.connect(process.env.DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log("DB Connected");
    } catch (e) {
        console.log('could not connect');
        console.log(e.message);
    }
};


connectToDb();

mongoose.Promise = global.Promise;