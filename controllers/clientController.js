const Client = require("../models/Client");
const Provider = require("../models/Provider");

exports.newClient = async (req, res) => {
    try {
        const clientExist = await Client.findOne({email: req.body.email});

        if (clientExist) {
            res.status(400).json({email: 'Client email already exist!'});
        }
        for (const providerId of req.body.providers) {
            const provider = await Provider.findById(providerId);
            if (!provider) {
                return res.status(404).json({message: 'Provider with ID ' + providerId + ' not found!'});
            }
        }
        
        const newClient = new Client({
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            providers: req.body.providers
        });
        await newClient.save();
        res.status(200).json({message: 'Client created successfully!', client: newClient});
    } catch (err) {
        res.status(400).json({error: err.message});
    }
}

exports.index = async (req, res) => {
    try {
        const clients = await Client.find().populate('providers');
        res.status(200).json(clients);
    } catch (err) {
        res.status(400).json({error: err.message});
    }
}

exports.show = async (req, res) => {
    try {
        const client = await Client.findById(req.params.id).populate('providers');
        if (!client) {
            res.status(404).json({message: 'Client not found!'})
        }
        res.status(200).json(client);
    } catch(err) {
        res.status(400).json({error: err.message});
    }
}

exports.update = async (req, res) => {
    try {
        const client = await Client.findById(req.params.id);
        if (!client) {
            return res.status(404).json({message: 'Client not found!'});
        }
        if (req.body.providers) {
            for (const providerId of req.body.providers) {
                const provider = await Provider.findById(providerId);
                if (!provider) {
                    return res.status(404).json({message: 'Provider with ID ' + providerId + ' not found!'});
                }
            }
        }
        await Client.findByIdAndUpdate(req.params.id, {
            name: req.body.name ? req.body.name : client.name,
            email: req.body.email ? req.body.email : client.email,
            phone: req.body.phone ? req.body.phone : client.phone,
            providers: req.body.providers
        });
        res.status(200).json({message: 'Client updated successfully!', client: await Client.findById(req.params.id)});
    } catch (err) {
        res.status(400).json({error: err.message});
    }
}

exports.destroy = async (req, res) => {
    try {
        const client = await Client.findById(req.params.id);
        if (!client) {
            res.status(404).json({message: 'Client not found!'});
        }
        await Client.findByIdAndDelete(req.params.id);
        res.status(200).json({message: 'Client deleted successfully!'});
    } catch (err) {
        res.status(400).json({error: err.message});
    }
}
