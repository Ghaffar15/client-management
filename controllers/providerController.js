const Provider = require("../models/Provider");

exports.store =  async (req, res) => {
    try {
        const provider = await Provider.findOne({name: req.body.name});

        if (provider) {
            return res.status(400).json({message: 'Provider name already exist!'});
        }
        
        const newProvider = new Provider({
            name: req.body.name
        });
        await newProvider.save();
        return res.status(200).json({message: 'Provider saved successfully!', provider: newProvider});

    }catch(err) {
        return res.status(400).json({error: err.message});
    }
}

exports.index = async (req, res) => {
    try {
        const providers = await Provider.find();
        return res.status(200).json(providers);
    } catch (err) {
        return res.status(400).json({error: err.message});
    }
}

exports.show = async (req, res) => {
    try {
        const provider = await Provider.findById(req.params.id);
        if (!provider) {
            return res.status(404).json({message: 'Provider not found!'});
        }
        return res.status(200).json(provider);
    } catch (err) {
        return res.status(400).json({error: err.message});
    }
}

exports.update = async (req, res) => {
    try {
        const provider = await Provider.findById(req.params.id);
        if (!provider) {
            return res.status(404).json({message: 'Provider not found!'});
        }
        await Provider.findByIdAndUpdate(req.params.id, {
            name: req.body.name ? req.body.name : provider.name
        });
        return res.status(200).json({message: 'Provider updated successfully!', provider: await Provider.findById(req.params.id)});
    } catch (err) {
        return res.status(400).json({error: err.message});
    }
}

exports.destroy = async (req, res) => {
    try {
        const provider = await Provider.findById(req.params.id);
        if (!provider) {
            return res.status(404).json({message: 'Provider not found!'});
        }
        await Provider.findByIdAndDelete(req.params.id);
        return res.status(200).json({message: 'Provider deleted succesfully!'});
    } catch (err) {
        return res.status(400).json({error: err.message});
    }
}